#!/bin/bash

# Global setup variables
export OMP_NUM_THREADS=1
export SINGULARITY_DISABLE_CACHE=1

# Loading software using spack
spack load -r openmpi target=$(spack arch -t)
spack load gcc

. /lustre/rz/dbertini/plasma/set_vp_epoch.sh

mpicc -showme:version
type gcc
type mpirun


echo " "
echo "SETUP--------------->"
echo " "

echo $PATH
echo $LD_LIBRARY_PATH

ulimit -c 0
#run the job
echo "." | srun -l --propagate=STACK,CORE --cpu-bind=verbose,cores \
		--distribution=block:cyclic --hint=multithread --  $EPOCH_ROOT/epoch2d/bin/epoch2d_centos
