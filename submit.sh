export NODELIST=/lustre/rz/dbertini/lwfa/epoch-virgo/nodelist_long
sbatch --ntasks 240 --ntasks-per-core 1 --no-requeue --job-name test_j --mem-per-cpu 4000 --mail-type ALL --mail-user d.bertini@gsi.de --partition main --time 0-08:00:00 -D ./data -o %j.out.log -e %j.err.log --exclude=$NODELIST -- ./run-file.sh
