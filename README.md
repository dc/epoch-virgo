# Epoch submit scripts
Example scripts use to submit EPOCH job (LWFA and TNSA) on the virgo cluster.
Detailed Information about the Virgo cluster can be found [here](https://hpc.gsi.de/virgo/preface.html) 

## Epoch installation
Epoch from the new [warwick git repository](https://github.com/Warwick-Plasma/epoch)
with last version tagged `v4.17.15`  is installed on `lustre-fs` at the location

`/lustre/rz/dbertini/plasma/epoch`

Epoch executable have been compiler for bare-metal as well as for debian8 and centos container.
The different flavours via added suffixes i.e

* `epoch1,2,3d_sys` for bare-metal (system)
* `epoch1,2,3d_deb8` for debian8  container
* `epoch1,2,3d_centos` for centos7 container

This installation is temporary and later on a dedicated `/cvmfs` should be used

## Submission scripts

*  `bare-metal`(no container) submission: `submit.sh` using `run-file.sh`
*  `debian8-container`: `submit_deb8.sh` using `run-file-deb8.sh`
*  `centos7-container`: `submit_centos.sh` using `run-file-centos.sh`

For example to launch a job on `centos-container` simply do
```
./submit_centos.sh

```


## Installing SDF Python library
Installing SDF python library is rather easy on the debian8 or centos containers using both
a quite recent version of python i.e version `3.7.7` as default.
In order to compile the `SDF Reader` one should first install the needed python module.  

1. After login to either `virgo-debian8` or `virgo-centos7`
load the basic packages using spack package manager: 

```
spack load -r openmpi target=$(spack arch -t) #loads openmpi@3.1.6  
spack load gcc # loads gcc@8.1.0
spack load python target=$(spack arch -t) # loads python@3.7.7 
```

1. Download and install `pip` installer
if not already done, you will need first to load a package manager for python
module ( here i will use `pip`)

```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py # download get-pip.py
python get-pip.py --user # install locally `pip` installer
```

This will install `pip` in `~/.local/bin/python3.7.7/`.

2. Install `numpy` and `nose` locally using `pip`

```
pip3.7 install numpy --user
pip3.7 install nose --user

```

This will install `numpy` and `nose` in `~/.local/lib/python3.7.7/site-packages/`.

3. We now can install the `SDF reader` Python module that comes with EPOCH.
 
 ```
 cd epoch/SDF/C
 make
 cd epoch/SDF/utilities
 which python # should return `spack` path to Python 3.7.7
 ./build
```

This will install `sdf` module in `~/.local/lib/python3.7.7/site-packages/`.

Be aware that all python binary and libs are install locally on your `$HOME` directory.
On virgo, only the submit nodes have a `$HOME` directory mounted.


## Example Python Analysis Script

In the directory `analysis` one can find a simple python analysis script
that reads SDF output file (LWFA simulation) using `sdf` module and fill
an binned histogramm  with simulated electron energy.
To run the example simply do

```
cd analysis
./3d_ana.py -f ../data/<DUMP_NR>.sdf

```
The histogram is saved in `PNG` format with the name `ana_<DUMP_NR>.png`.


## Communication Optimised submit scripts
New submit scripts offers more optimised MPI process placement. It is a tradeoff between the nearly
unpractical `--exclusive` and the default `SLURM` process distribution (no option used) which can often
be sub-optimal.
The scripts are provided for all systems:

* `submit_compact.sh` for bare-metal (system)
* `submit_compact_deb8.sh` for debian8  container
* `submit_compact_centos.sh` for centos7 container

and make use of the `SLURM` commands `--nodes N  --ntasks-per-node M` in order to force the usage of **fixed**
number of physical nodes used for the Epoch job.
The idea is simple: one should reduce the communication between nodes.
For that puprose, fixing the number of nodes will avoid the case when too many physical nodes are selected.

For example choosing `--nodes 64 --ntasks-per-node 16` instead of the unique option `--ntaks 1024` on can get a speedup
of more that a **factor 5** for a typical TNSA simulation !

Furthermore, the `bash` scripts
* `run-file.sh` for bare-metal (system)
* `run-file-deb8.sh` for debian8  container
* `run-file-centos.sh` for centos7 container

shows additionnal options that one use when executing the `srun` command to specify a specific pattern
for the binding and distribution of MPI taks on the cluster.

To specify such a pattern the scripts uses now the  commands `--cpu_bind` and `--distribution`.

*`--cpu-bind=<cores,sockets>` defines the resolution in which the tasks will be allocated
*`--distribution=<block|cyclic>` determines the order in which the tasks will be allocated to the cpus

More detailed information on the low-level SLURM interface can be found [here](https://slurm.schedmd.com/srun.html).


## New `cvmfs` repository `phelix.gsi.de` 
A new `cvmfs` repository for software installation relevant for the plasma physics is now available.
It is recommended to use/test this new repository for your simulation.

### Repository structure
For now the repository contains the `epoch1d,2d,3d` binaries as well as the `SDF` python modules
installed for `python3.7`.

```
/cvmfs/phelix.gsi.de$ 
.
├── epoch
│   └── 4.17.16
│       ├── gcc
│       │   └── ompi3.1_gcc8_centos
│       └── py
│           ├── bin
│           ├── lib
│           │   └── python3.7
│           │       └── site-packages
│           │           ├── cycler-0.10.0.dist-info
│           │           ├── dateutil
│           │           ├── kiwisolver-1.3.1.dist-info
│           │           ├── matplotlib
│           │           ├── matplotlib-3.4.3.dist-info
│           │           ├── mpl_toolkits
│           │           ├── nose
│           │           ├── nose-1.3.7.dist-info
│           │           ├── numpy
│           │           ├── numpy-1.21.2.dist-info
│           │           ├── numpy.libs
│           │           ├── PIL
│           │           ├── Pillow-8.3.1.dist-info
│           │           ├── Pillow.libs
│           │           ├── __pycache__
│           │           ├── pyparsing-2.4.7.dist-info
│           │           ├── python_dateutil-2.8.2.dist-info
│           │           ├── sdf_helper
│           │           ├── six-1.16.0.dist-info
│           │           ├── wheel
│           │           └── wheel-0.37.0.dist-info
│           └── man
│               └── man1
├── modulefiles
│   └── epoch
│       └── centos_gcc
└── modulefiles_src

```

### Submit scripts
2 dedicated submit script examples show how to use the new repository via
the standard `Linux Environment Module`:

```
# Use now module to load Epoch environment

module use /cvmfs/phelix.gsi.de/modulefiles
module load epoch/centos_gcc/4.17.16_mpi3


#  Module name convention:   
#     epoch/linux_arch_container_compiler/epoch_version_mpi_version
#  So in this case we will use
#                  -  centos7 container
#                  -  gcc compiler (version 8.1.0 default in HPC_containers)  
#                  -  epoch version 4.17.16
                  
```


* `run-file-cvmfs-centos.sh` for `centos7` container
* `submit_cvmfs_centos.sh` for the submission.



## Known Problems

### Using containers
Running Epoch within a container environnment generate the following error message
```
WARNING: Cache disabled - cache location / is not writable.
```
This message can be ignored and even removed by setting
```
export SINGULARITY_DISABLE_CACHE=1 
```
in the corresponding sbatch script as done here in the submit examples.



