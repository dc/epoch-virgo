export NODELIST=/lustre/rz/dbertini/lwfa/johannes/nodelist_long
sbatch --ntasks 512 --ntasks-per-core 1 --no-requeue --job-name tnsa_j --mem-per-cpu 4000 --mail-type ALL --mail-user d.bertini@gsi.de --partition long --time 7-00:00:00 -D ./data -o %j.out.log -e %j.err.log --exclude=$NODELIST  -- ./run-file-cvmfs-centos.sh

