#!/bin/bash
export OMP_NUM_THREADS=1

. /lustre/rz/dbertini/plasma/set_vp_epoch.sh

mpicc -showme:version
type gcc
type mpirun

echo " "
echo "SETUP--------------->"
echo " "

echo $PATH
echo $LD_LIBRARY_PATH
type epoch2d_sys

ulimit -c 0

#run the job
echo "." | srun -l --propagate=STACK,CORE --cpu-bind=cores \
  --distribution=block:cyclic --hint=multithread -- $EPOCH_ROOT/epoch2d/bin/epoch2d_sys
