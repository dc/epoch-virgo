# Load needed packages
unset LD_LIBRARY_PATH

echo "spack loading openmpi"
spack load  openmpi target=$(spack arch -t)    
echo "spack loading gcc"
spack load gcc    
echo "spack loading python"
spack load python target=$(spack arch -t)    
echo "spack loading cmake"
spack load  cmake target=$(spack arch -t)    

# Settings for Epoch (cvmfs)
export EPOCH_ROOT=/cvmfs/phelix.gsi.de/epoch/4.17.16/gcc/ompi3.1_gcc8_centos
export PATH=$EPOCH_ROOT/:$PATH

# Use local HDF5
export HDF5_ROOT=/cvmfs/phelix.gsi.de/hdf5/1.12.1/gcc/ompi3.1_gcc8_centos
export LD_LIBRARY_PATH=${HDF5_ROOT}/lib:$LD_LIBRARY_PATH
export PATH=${HDF5_ROOT}/bin:$PATH

echo "Epoch setup: "
type gcc
type mpicc
type python
type h5dump

type epoch1d
type epoch2d
type epoch3d


