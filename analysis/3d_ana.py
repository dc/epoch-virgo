#!/usr/bin/env python3

#
# Example of analysis of 3D Epoch LWFA produced data 
#
# (The SDF python Module is used to analyse Epoch Data.)
#
#
#  input.deck used: input.deck.lwfa 
#
#  Output: Binned Histogram for Electron Energy 
#
# How to run:
#                ./3d_ana.py -f <VAL>.sdf  
#

import sdf
import matplotlib.pyplot as plt
import numpy as np
import sys
import os
import argparse


def get_extension(full_path_to_file):
    fname = os.path.basename(full_path_to_file)
    base_filename, ext = os.path.splitext(fname)
    return base_filename, ext

def load_sdf(args):
    data_3d = sdf.read(args.load_file, dict=True)

    # normalization factor, m*c in units of kg m/s.
    norm = 2.73092449e-22

    # particle momenta
    px = data_3d['Particles/Px/' + args.species].data / norm
    py = data_3d['Particles/Py/' + args.species].data / norm
    pz = data_3d['Particles/Pz/' + args.species].data / norm

    # particle weights
    w = data_3d['Particles/Weight/' + args.species].data

    # filter electrons with certain momenta
    if args.filter:
        print('Filtering particle momenta.')
        mask = ((px > 2.) & ((py**2 + pz**2) / px**2 < 1.))
        px, py, pz, w = [arr[mask] for arr in (px, py, pz, w)]

    # compute energy from momentum components
    ene = .511 * (np.sqrt(1. + px**2 + py**2 + pz**2) - 1.)

    H, edges = np.histogramdd(ene, bins=100, weights=w)

    return edges[0][:-1], H # energy, count

def plot(energy, number, suffix):
    fig, ax = plt.subplots()
    fig.subplots_adjust(left=.15, bottom=.16, right=.99, top=.97)
    plot_kwds={'linewidth':5, 'alpha':0.5}
    ax.semilogy(energy, number, basey=10, nonposy='mask', **plot_kwds)
    ax.grid(color='gray', linestyle='--', linewidth=2)
    ax.set_xlabel('Energy [MeV]')
    ax.set_ylabel('Counts [Number per MeV]')
    fig.savefig(f'ana_{suffix}.png')
    plt.close(fig)


    

def main():
    print("Beginning analysis")
    parser = argparse.ArgumentParser(
        description="this script loads electron spectrum from .sdf or .txt file, "
                    "saves to .txt file and plots spectrum")
    parser.add_argument(
        '-f', '--load-file',
        type=str,
        required=True,
        help="The sdf/txt file to load.")
    parser.add_argument(
        '--species',
        type=str,
        default='subset_testparticle/electront',
        help="Name of the electron species in the .sdf file.")
    parser.add_argument(
        '--filter',
        action='store_true',
        help="Filter electrons with certain momenta.")

    print('Args analysis parsed')
    
    args = parser.parse_args() # load command line args
    base_fname, ext = get_extension(args.load_file) # separate file name from extension

    print('name of the output_file:', base_fname)
    
    if ext == '.sdf':
        x, y = load_sdf(args) # read 
        np.savetxt(base_fname + '.txt', np.c_[x,y])
    elif ext == '.txt':
        xy = np.loadtxt(args.load_file)
        x = xy[:,0] # unpack columns
        y = xy[:,1]
    else:
        parser.error('The filename extension must be either .sdf of .txt')

    print('name of the output_file:', base_fname)
        
    plot(x, y, base_fname)


if __name__ == "__main__":
    main()
