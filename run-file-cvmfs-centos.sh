#!/bin/bash
export OMP_NUM_THREADS=1



export SINGULARITY_DISABLE_CACHE=1

# Use now module to load Epoch environment

module use /cvmfs/phelix.gsi.de/modulefiles
module load epoch/centos_gcc/4.17.16_mpi3


#  Module name convention:   
#     epoch/linux_arch_container_compiler/epoch_version_mpi_version
#  So in this case we will use
#                  -  centos7 container
#                  -  gcc compiler (version 8.1.0 default in HPC_containers)  
#                  -  epoch version 4.17.16
                  

mpicc -showme:version
type gcc
type mpicc
type mpirun
type srun


echo " "
echo "Epoch version 4.17.16  Cluster Setup initialised>>>"
echo " "

echo $PATH
echo $LD_LIBRARY_PATH



ulimit -c 0

echo "." | srun -l --propagate=STACK,CORE --cpu-bind=verbose,cores \
		--distribution=block:cyclic --hint=multithread --  epoch2d


